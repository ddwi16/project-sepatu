using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectObject : MonoBehaviour
{
  [SerializeField] ChangeMaterialColor changeMaterial;

  // Update is called once per frame
  void Update()
  {
    if (Input.GetMouseButtonDown(0))
    {
      Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

      if (Physics.Raycast(ray, out RaycastHit hitInfo, 100f))
      {
        print(hitInfo.collider.gameObject);
        if (hitInfo.collider.gameObject.tag == "body")
        {
          changeMaterial.TriggerChangeBodyMat();
        }
        else if (hitInfo.collider.gameObject.tag == "mid")
        {
          changeMaterial.TriggerChangeSuedeMat();
        }
        else if (hitInfo.collider.gameObject.tag == "sole")
        {
          changeMaterial.TriggerChangeSoleMat();
        }
      }
    }
  }
}
