using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ChangeMaterialColor : MonoBehaviour
{
  [SerializeField] GameObject[] shoesBody, shoesSole, shoesSuede;
  [SerializeField] Material blackMat, whiteMat;
  [SerializeField] GameObject btnBlackMat, btnWhiteMat;
  [SerializeField] TextMeshProUGUI statusSelectedTxt;
  bool isBody, isSuede, isSole;

  private void Update()
  {
    if (Input.GetKeyDown(KeyCode.Escape))
    {
      Application.Quit(0);
    }
  }

  public void TriggerChangeBodyMat()
  {
    statusSelectedTxt.text = "Body Selected";
    btnBlackMat.SetActive(true);
    btnWhiteMat.SetActive(true);
    isBody = true;
    isSuede = false;
    isSole = false;
  }

  public void TriggerChangeSuedeMat()
  {
    statusSelectedTxt.text = "Suede Selected";
    btnBlackMat.SetActive(true);
    btnWhiteMat.SetActive(true);
    isBody = false;
    isSuede = true;
    isSole = false;
  }

  public void TriggerChangeSoleMat()
  {
    statusSelectedTxt.text = "Sole Selected";
    btnBlackMat.SetActive(true);
    btnWhiteMat.SetActive(true);
    isBody = false;
    isSuede = false;
    isSole = true;
  }

  public void ChangeBodyBlack()
  {
    if (isBody)
    {
      foreach (GameObject body in shoesBody)
      {
        body.GetComponent<MeshRenderer>().material = blackMat;
      }
    }
    else if (isSuede)
    {
      foreach (GameObject suede in shoesSuede)
      {
        suede.GetComponent<MeshRenderer>().material = blackMat;
      }
    }
    else if (isSole)
    {
      foreach (GameObject sole in shoesSole)
      {
        sole.GetComponent<MeshRenderer>().material = blackMat;
      }
    }
  }

  public void ChangeBodyWhite()
  {
    if (isBody)
    {
      foreach (GameObject body in shoesBody)
      {
        body.GetComponent<MeshRenderer>().material = whiteMat;
      }
    }
    else if (isSuede)
    {
      foreach (GameObject suede in shoesSuede)
      {
        suede.GetComponent<MeshRenderer>().material = whiteMat;
      }
    }
    else if (isSole)
    {
      foreach (GameObject sole in shoesSole)
      {
        sole.GetComponent<MeshRenderer>().material = whiteMat;
      }
    }
  }
}
