using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateBehavior : MonoBehaviour
{
  [SerializeField] float speedMove;
  Rigidbody _rigidbody;

  private void Start()
  {
    _rigidbody = GetComponent<Rigidbody>();
  }

  private void OnMouseDrag()
  {
    float x = Input.GetAxis("Mouse X") * speedMove * Time.fixedDeltaTime;
    float y = Input.GetAxis("Mouse Y") * speedMove * Time.fixedDeltaTime;

    _rigidbody.AddTorque(Vector3.down * x);
    _rigidbody.AddTorque(Vector3.right * y);
  }
}
